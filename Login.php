<?php

require_once('./vendor/autoload.php');

use Firebase\JWT\JWT;
use Dotenv\Dotenv;

// Check If Session Has Been Started Then Start A Session
if (session_status() === PHP_SESSION_NONE) session_start();

// Validation Request Method For This File, Required POST Method
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    http_response_code(405);
    echo json_encode([
       'state' => false,
       'message' => 'Method Not Allowed',
       'data' => []
    ]);
    exit();
}

// Initiate Package Dotenv for call .env file
$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

// Initiate header file
header('Content-Type: application/json');

// Get All Request Input
$json = file_get_contents('php://input');
$input = json_decode($json);

// Validation Input NIK, and Password is available
if (!isset($input->nik) || !isset($input->password)) {
    http_response_code(400);
    echo json_encode([
        'state' => false,
        'message' => 'Variable Is Missing',
        'data' => []
    ]);
    exit();
}

// Validation minimun length password 6 character
if(strlen($input->password) < 6) {
    http_response_code(400);
    echo json_encode([
        'state' => false,
        'message' => 'Password minimun has 6 character.',
        'data' => []
    ]);
    exit();
}

// Validation minimun length NIK 16 character
if(strlen($input->nik) < 16) {
    http_response_code(400);
    echo json_encode([
        'state' => false,
        'message' => 'NIK minimun has 16 character.',
        'data' => []
    ]);
    exit();
}

// Process User Login
if (isset($_SESSION["user"])) {
    // Process finding existing nik
    for ($i = 0; $i < count($_SESSION["user"]); $i++){
        if  ($_SESSION["user"][$i]['nik'] === $input->nik && $_SESSION["user"][$i]['password'] === md5($input->password)) {
            $_SESSION["auth"]['id'] = $i;
            $_SESSION["auth"] = $_SESSION["user"][$i];
            break;
        } else {
            http_response_code(400);
            echo json_encode([
                'state' => false,
                'message' => 'NIK or password is wrong.',
                'data' => []
            ]);
            exit();
        }
    }
}

// Result endpoint
echo json_encode($_SESSION["auth"]);